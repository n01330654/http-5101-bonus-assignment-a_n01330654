﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="String Bling.aspx.cs" Inherits="bonus_assignment_2_a_n01330654.String_Bling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <p><strong>STRING BLING</strong></p>
        <asp:Label runat="server" ID="labelInput" text="Checking if the word is a PALINDROME:"></asp:Label>
        <br />
        <asp:TextBox runat="server" ID="inputString" placeholder="Enter a word"></asp:TextBox>
        <asp:RegularExpressionValidator ID="validateInputString" runat="server" ValidationExpression="^[a-zA-Z]+$" ControlToValidate="inputString" ErrorMessage="Please enter a word"></asp:RegularExpressionValidator>
        <br /><br />
        <asp:Button runat="server" ID="checkPalindrome" Onclick="CheckPalindrome_Click" text="Find Out"/>
        
        <br /><br />
        <div id="palindrome" runat="server"></div>
        </div>
    </form>
</body>
</html>
