﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cartesian-Smartesian.aspx.cs" Inherits="bonus_assignment_2_a_n01330654.Cartesian_Smartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <p><strong>CARTESIAN SMARTESIAN</strong></p>
        <asp:Label runat="server" ID="xLabel" text="Please Enter your X value:"></asp:Label>
        <br />
        <asp:TextBox runat="server" ID="xValue" placeholder="Enter a valid number"></asp:TextBox>
        <asp:CustomValidator ID="xValueValidator" runat="server" ErrorMessage="Enter a number except zero" ControlToValidate="xValue" OnServerValidate="InputValueOfX_Validator" ></asp:CustomValidator>
        <br /><br />
        <asp:Label runat="server" ID="yLabel" text="Please Enter your Y value:"></asp:Label>
        <br />
        <asp:TextBox runat="server" ID="yValue" placeholder="Enter a valid number"></asp:TextBox>
        <asp:CustomValidator runat="server" ErrorMessage="Enter a number except zero" ControlToValidate="yValue" OnServerValidate="InputValueOfY_Validator" ></asp:CustomValidator>
        <br /><br />
        <asp:Button runat="server" ID="checkQuadrant" Onclick="CheckQuadrant_Click" text="Find Out"/>
        <br /><br />
        <div id="quadrant" runat="server"></div>
        </div>
    </form>
</body>
</html>
