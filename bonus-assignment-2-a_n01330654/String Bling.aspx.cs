﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bonus_assignment_2_a_n01330654
{
    public partial class String_Bling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CheckPalindrome_Click(object sender, EventArgs e)
        {
            string inputWord = inputString.Text.ToString();
            string inputWordLowerCase = inputWord.ToLower();
            string inputWordRemoveSpaces = inputWordLowerCase.Replace(" ", string.Empty);
            int inputLength = inputWordRemoveSpaces.Length;
            int i;
            string checkInput = "";

            for (i = inputLength - 1; i >= 0; i--)
                {
                checkInput = checkInput + inputWordRemoveSpaces[i];
            }

            if (checkInput == inputWordRemoveSpaces)
            {
                palindrome.InnerHtml = inputString.Text +  " is a PALINDROME!";
            }
            else
            {
                palindrome.InnerHtml = inputString.Text + " is not a PALINDROME!";
            }

        }
    }
}