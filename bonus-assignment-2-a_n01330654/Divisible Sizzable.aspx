﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Divisible Sizzable.aspx.cs" Inherits="bonus_assignment_2_a_n01330654.Divisible_Sizzable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <p><strong>DIVISIBLE SIZZABLE</strong></p>
        <asp:Label runat="server" ID="checkNumber" text="Checking if its a Prime Number:"></asp:Label>
        <br />
        <asp:TextBox runat="server" ID="inputNumber" placeholder="Enter a valid number"></asp:TextBox>
        <asp:RangeValidator ID="validatorInputNumber" runat="server" ControlToValidate="inputNumber" Type="Integer" MinimumValue="2" MaximumValue="9999" ErrorMessage="Enter a valid number from (2 - 999)"></asp:RangeValidator>
        <br /><br />
        <asp:Button runat="server" ID="checkPrimeNumber" Onclick="checkPrimeNumber_Click" text="Find Out"/>
        <br /><br />
        <div id="checkPrime" runat="server"></div>
        </div>
    </form>
</body>
</html>
