﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bonus_assignment_2_a_n01330654
{
    public partial class Cartesian_Smartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void InputValueOfX_Validator(object source, ServerValidateEventArgs args)
        {
            int valueOfX = int.Parse(xValue.Text);
      
            if (valueOfX == 0)
            {
                quadrant.InnerHtml = "";
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void InputValueOfY_Validator(object source, ServerValidateEventArgs args)
        {
            int valueOfY = int.Parse(yValue.Text);

            if (valueOfY == 0)
            {
                quadrant.InnerHtml = "";
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void CheckQuadrant_Click(object sender, EventArgs e)
        {
            int valueOfX = int.Parse(xValue.Text);
            int valueOfY = int.Parse(yValue.Text);

            if (valueOfX > 0 && valueOfY > 0)
            {
                quadrant.InnerHtml = "x-axis: " + valueOfX + "</br>" + " " + "y-axis: " + valueOfY + "</br>" + "</br>" + " Values given is in Quadrant I";
            }
            else if(valueOfX < 0 && valueOfY > 0)
            {
                quadrant.InnerHtml = "x-axis: " + valueOfX + "</br>" + " " + "y-axis: " + valueOfY + "</br>" + "</br>" + " Values given is in Quadrant II";
            }
            else if (valueOfX < 0 && valueOfY < 0)
            {
                quadrant.InnerHtml = "x-axis: " + valueOfX + "</br>" + " " + "y-axis: " + valueOfY + "</br>" + "</br>" + " Values given is in Quadrant III";
            }
            else if(valueOfX > 0 && valueOfY < -1)
            {
                quadrant.InnerHtml = "x-axis: " + valueOfX + "</br>" + " " + "y-axis: " + valueOfY + "</br>" + "</br>" + " Values given is in Quadrant IV";
            }
        }
    }
}